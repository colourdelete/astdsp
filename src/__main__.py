# import vlc
from pydub import AudioSegment


def mix(segments=[]):
  '''Put the longest track first!!!segments=[[audio, db], ...]'''
  audio = AudioSegment.empty()
  for i in range(len(segments)):
    segment = segments[i]
    result = segment[0].set_channels(1) + segment[2]
    pan_amount = segment[1]/180
    if pan_amount == 1:
      pan_amount = 0
    elif pan_amount > 1:
      pan_amount = pan_amount%1*-1 # flip
    pan_amount *= 2
    result = result.pan(pan_amount)
    if i == 0:
      audio = result
    else:
      audio = audio.overlay(result)
  return audio

if __name__ == '__main__':
  print('AstDSp')
  segments = [
    [AudioSegment.from_file('./track0.mp3') + AudioSegment.silent(13000), 0, 0],
    [AudioSegment.silent(3000) + AudioSegment.from_file('./track1.mp3'), 90, -10],
    [AudioSegment.silent(6000) + AudioSegment.from_file('./track2.mp3'), 180, +10],
    [AudioSegment.silent(9000) + AudioSegment.from_file('./track3.mp3'), 270, 0],
  ]
  print('Mixing...')
  mixed = mix(segments)
  print('Exporting...')
  mixed.export('mixed.mp3')
  print('Done.')